import { Component } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  data: [string, number];
  wasRequest: boolean = false;

  constructor(private dataService: DataService) {}


  getWeather() {
    this.dataService.getWeather().subscribe(data => {
      this.data = data;
      this.wasRequest = true;
    });
  }
}
