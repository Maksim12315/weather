import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private httpClient: HttpClient) {
    this.getUrl();
  }
  // install for cors(from http to https) request https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en-US 
  private url = 'https://api.darksky.net/forecast/'; // [key]/[latitude],[longitude]
  private key = '6faa2a967cb47f06f8757620118b4090';
  private fullUrl: string;
  existUrl: boolean = false;

  

  getUrl() {
    if (navigator.geolocation) {
      const geoSuccess = (position) => {
        this.fullUrl = `${this.url}${this.key}/${position.coords.latitude},${position.coords.longitude}`;
        this.existUrl = true;
      };
      navigator.geolocation.getCurrentPosition(geoSuccess)
    }
  }

  getWeather(): Observable<any> {
    return this.httpClient.get(this.fullUrl).pipe(map(data => {
      let degCel = this.farToCel(data['currently']['temperature']);
      return [data['currently']['icon'], degCel];
    }));
  }

  farToCel(deg: number): number {
    return Math.round((deg-32)/1.8);
  }


}
